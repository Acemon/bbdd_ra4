package com.fernando.mmo.gui;

import com.fernando.mmo.util.*;
import com.fernando.mmo.base.*;
import com.fernando.mmo.base.Magias;
import org.bson.types.ObjectId;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by DAM on 03/11/2016.
 */
public class Controller implements ActionListener, ChangeListener, ListSelectionListener, KeyListener{
    private Model model;
    private Ventana view;
    private Integer posicion;
    private boolean nuevo;
    private ObjectId clave;
    private boolean campoVacio;

    public Controller(Model model, Ventana view) {
        this.model=model;
        this.view=view;
        this.nuevo=false;
        addListeners();
        addActionCommands();
        try {
            System.out.println(Constantes.DEF_RUTA);
            model.comprobacionFileConfig(Constantes.DEF_RUTA);
        } catch (IOException e) {
            Util.MensajeError("Error al generar el PROPS");
        }
        activarInicial();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Usuario usuario = null;
        Clase clase = null;
        Raza raza = null;
        Mundo mundo=null;
        String ruta = "";
        switch (e.getActionCommand()){
            case "Cambiar":
                if (view.rbPostgre.isSelected())
                    Constantes.BASE_DATOS="Postgre";
                else
                    Constantes.BASE_DATOS="MySQL";
                try {
                    model.crearProps(Constantes.DEF_RUTA);
                } catch (IOException e1) {
                    Util.MensajeError("Error al crear el archivo props");
                }
                break;
            case "Conectar":
                String contrasena = new String(view.tfContrase.getPassword());
                try {
                    switch (model.comprobarUserPwd(view.tfUsuario.getText(), contrasena)){
                        case "admin":
                            view.lbbdd.setVisible(true);
                            view.rbMysql.setVisible(true);
                            view.rbPostgre.setVisible(true);
                            view.btCambiar.setVisible(true);
                        case "usuario":
                            model.conectarBBDD();
                            refrescardlm();
                            view.btConectar.setEnabled(false);
                            view.tfUsuario.setEnabled(false);
                            view.tfContrase.setEnabled(false);
                            view.tabbedPane1.setEnabled(true);
                            activarCampos(false);
                            view.laviso.setText("Conectado");
                            break;
                    }
                } catch (IOException e1) {
                    Util.MensajeError("Error al acceder al PROPS");
                }
                break;
            case "Anadir":
                view.btModificar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.tabbedPane1.setEnabled(false);
                activarCampos(true);
                limpiarTablas();
                refrescarcb();
                nuevo=true;
                view.laviso.setText("Modo Añadir");
                break;
            case "Modificar":
                view.btModificar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.tabbedPane1.setEnabled(false);
                activarCampos(true);
                nuevo=false;
                view.laviso.setText("Modo Modificar");
                break;
            case "Guardar":
                campoVacio=false;
                //Usuario
                if (posicion==0){
                    usuario= new Usuario();
                    usuario=rellenar(usuario);
                    if (campoVacio){
                        Util.MensajeError("Faltan datos");
                        break;
                    }else {
                        if (!nuevo){
                            usuario.setId(clave);
                            model.modificarUsuario(usuario);
                        }
                        else
                            model.registrar(usuario);
                    }
                //Clase
                }else if (posicion==1){
                    clase = new Clase();
                    clase=rellenar(clase);
                    if (campoVacio){
                        Util.MensajeError("Faltan datos");
                        break;
                    }else{
                        if (!nuevo){
                            clase.setId(clave);
                            model.modificarClase(clase);
                        }
                        else
                            model.registrar(clase);
                    }
                //Raza
                }else if (posicion==2){
                    raza = new Raza();
                    raza=rellenar(raza);
                    if (campoVacio){
                        Util.MensajeError("Faltan datos");
                        break;
                    }else{
                        if (!nuevo){
                            raza.setId(clave);
                            model.modificarRaza(raza);
                        }
                        else
                            model.registrar(raza);
                    }
                //Mundo
                }else if (posicion==3){
                    mundo = new Mundo();
                    mundo = rellenar(mundo);
                    if (campoVacio){
                        Util.MensajeError("Faltan datos");
                        break;
                    }else{
                        if (!nuevo){
                            mundo.setId(clave);
                            model.modificarMundo(mundo);
                        }
                        else
                            model.registrar(mundo);
                    }
                }
                view.tabbedPane1.setEnabled(true);
                limpiarTablas();
                limpiarcb();
                refrescardlm();
                activarCampos(false);
                view.btGuardar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.laviso.setText("Guardado");
                break;
            case "Cancelar":
                view.tabbedPane1.setEnabled(true);
                limpiarTablas();
                limpiarcb();
                refrescardlm();
                activarCampos(false);
                view.btGuardar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.laviso.setText("Guardado");
                break;
            case "Eliminar":
                if (posicion==0){
                    usuario = new Usuario();
                    usuario = rellenar(usuario);
                    usuario.setId(clave);
                    System.out.println(clave);
                    model.eliminarUsuario(usuario);
                }else if (posicion==1){
                    clase = new Clase();
                    clase = rellenar(clase);
                    clase.setId(clave);
                    model.eliminarClase(clase);
                }else if (posicion==2){
                    System.out.println(clave);
                    raza = new Raza();
                    raza = rellenar(raza);
                    raza.setId(clave);
                    model.eliminarRaza(raza);
                } else if (posicion==3){
                    mundo = new Mundo();
                    mundo = rellenar(mundo);
                    mundo.setId(clave);
                    model.eliminarMundo(mundo);
                }
                activarCampos(false);
                refrescardlm();
                view.laviso.setText("Eliminado");
                view.btGuardar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                break;
            case "Registrar":
                view.dlmMunUsuario.addElement(view.cbMunJugadores.getSelectedItem());
                break;
            case "Quitar":
                view.dlmMunUsuario.removeElement(view.lMunPersonas.getSelectedValue());
                break;
        }
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        if (changeEvent.getSource()==view.chkMagia){
            if (view.chkMagia.isSelected()){
                view.cbClaMagia.setVisible(true);
                view.lmagia.setVisible(true);
            }else{
                view.cbClaMagia.setVisible(false);
                view.lmagia.setVisible(false);
            }
        }else{
            posicion=view.tabbedPane1.getSelectedIndex();
            view.cbBuscador.removeAllItems();
            if (posicion==0){
                view.lBuscador.setModel(view.dlmUsuario);
                view.cbBuscador.addItem("nombre");
                view.cbBuscador.addItem("apellido");
                view.cbBuscador.addItem("fecha");
            }else if (posicion==1){
                view.lBuscador.setModel(view.dlmClase);
                view.cbBuscador.addItem("clase");
                view.cbBuscador.addItem("salud");
                view.cbBuscador.addItem("armadura");
                view.cbBuscador.addItem("magias");
            }else if (posicion==2){
                view.lBuscador.setModel(view.dlmRaza);
                view.cbBuscador.addItem("raza");
                view.cbBuscador.addItem("skill_1");
                view.cbBuscador.addItem("skill_2");
                view.cbBuscador.addItem("skill_ex");
            }else if (posicion==3){
                view.lBuscador.setModel(view.dlmMundo);
                view.cbBuscador.addItem("nombre");
            }
            limpiarcb();
            view.btModificar.setEnabled(false);
            view.btEliminar.setEnabled(false);
        }

    }

    @Override
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        if(listSelectionEvent.getValueIsAdjusting()){
            return;
        }
        if (!listSelectionEvent.getSource().equals(view.lMunPersonas)){
            refrescarcb();
            limpiarTablas();
            if (posicion==0){
                Usuario usuario = (Usuario) view.lBuscador.getSelectedValue();
                if (usuario==null)
                    return;

                clave=usuario.getId();
                view.tfUsuNombre.setText(usuario.getNombre());
                view.tfUsuApellido.setText(usuario.getApellido());
                view.tfUsuFecha.setDate(usuario.getFecha());
                view.cbUsuRaza.setSelectedItem(usuario.getRaza());
                view.cbUsuClase.setSelectedItem(usuario.getClase());
                view.dlmUsuMundo.removeAllElements();

                for (int i=0; i<usuario.getMundos().size(); i++){
                    view.dlmUsuMundo.addElement(
                            model.obtenerMundo(usuario.getMundos().get(i)));
                }

            }else if (posicion==1){
                Clase clase = (Clase) view.lBuscador.getSelectedValue();
                if (clase==null)
                    return;

                clave=clase.getId();
                view.tfClaNombre.setText(clase.getClase());
                view.tfClaVida.setText(String.valueOf(clase.getSalud()));
                if (clase.getArmadura().equals(Clase.Armadura.Ligera))
                    view.rbLigera.setSelected(true);
                else if (clase.getArmadura().equals(Clase.Armadura.Media))
                    view.rbMedia.setSelected(true);
                else
                    view.rbPesada.setSelected(true);
                if (clase.isMagia()){
                    view.chkMagia.setSelected(true);
                    view.cbClaMagia.setSelectedItem(clase.getMagias());
                }

            }else if (posicion==2){
                Raza raza = (Raza) view.lBuscador.getSelectedValue();
                if (raza==null)
                    return;

                clave=raza.getId();
                view.tfRazNom.setText(raza.getRaza());
                view.tfRazSkill1.setText(raza.getSkill1());
                view.tfRazSkill2.setText(raza.getSkill2());
                view.tfRazSkill3.setText(raza.getSkill3());
                view.cbRazSkill.setSelectedItem(raza.getSkillEx());

            } else if (posicion==3){
                Mundo mundo = (Mundo) view.lBuscador.getSelectedValue();
                if (mundo==null)
                    return;

                clave=mundo.getId();
                view.tfMunNombre.setText(mundo.getNombre());
                view.dlmMunUsuario.removeAllElements();
                for (int i=0; i<mundo.getUsuarios().size(); i++){
                    view.dlmMunUsuario.addElement(
                            model.obtenerUsuario(mundo.getUsuarios().get(i)));
                }

            }
            if (!view.tfUsuNombre.getText().equals("") || !view.tfClaNombre.getText().equals("") ||
                    !view.tfRazNom.getText().equals("") || !view.tfMunNombre.getText().equals("")){
                view.btModificar.setEnabled(true);
                view.btEliminar.setEnabled(true);
            }
        }
    }

    private Usuario rellenar(Usuario usuario) {
        usuario.setNombre(view.tfUsuNombre.getText());
        usuario.setApellido(view.tfUsuApellido.getText());
        usuario.setFecha(view.tfUsuFecha.getDate());
        usuario.setRaza((Raza) view.cbUsuRaza.getSelectedItem());
        usuario.setClase((Clase) view.cbUsuClase.getSelectedItem());
        if (usuario.getNombre().equals("") || usuario.getApellido().equals("") || usuario.getFecha().equals("")){
            this.campoVacio=true;
        }
        ArrayList<ObjectId>mundos = new ArrayList<>();
        for (int i=0; i<view.dlmUsuMundo.getSize(); i++){
            Mundo mundo = (Mundo) view.dlmUsuMundo.getElementAt(i);
            mundos.add(mundo.getId());
        }
        usuario.setMundos(mundos);
        return usuario;
    }

    private Clase rellenar(Clase clase) {
        clase.setClase(view.tfClaNombre.getText());
        if (clase.getClase().equals("")){
            campoVacio=true;
        }
        if (view.tfClaVida.getText()==""){
            clase.setSalud((double) 0);
        }else {
            try{
                clase.setSalud(Double.parseDouble(view.tfClaVida.getText()));
            }catch (Exception e){
                this.campoVacio=true;
                Util.MensajeError("Has introducido una cadena de texto en un campo numerico");
            }
        }
        if (view.rbLigera.isSelected()) {
            clase.setArmadura(Clase.Armadura.Ligera.toString());
        }else if (view.rbMedia.isSelected()) {
            clase.setArmadura(Clase.Armadura.Media.toString());
        }else if (view.rbPesada.isSelected()){
            clase.setArmadura(Clase.Armadura.Pesada.toString());
        }else{
            Util.MensajeError("No has seleccionado una armadura");
            this.campoVacio=true;
        }
        clase.setMagia(view.chkMagia.isSelected());
        if (clase.isMagia()){
            clase.setMagias(view.cbClaMagia.getSelectedItem().toString());
        }else{
            clase.setMagias(null);
        }
        return clase;
    }

    private Raza rellenar(Raza raza) {
        raza.setRaza(view.tfRazNom.getText());
        raza.setSkill1(view.tfRazSkill1.getText());
        raza.setSkill2(view.tfRazSkill2.getText());
        raza.setSkill3(view.tfRazSkill3.getText());
        raza.setSkillEx(view.cbRazSkill.getSelectedItem().toString());
        if (raza.getRaza().equals("") || raza.getSkill1().equals("") ||
                raza.getSkill2().equals("") || raza.getSkill3().equals("")){
            this.campoVacio=true;
        }
        return raza;
    }

    private Mundo rellenar (Mundo mundo){
        mundo.setNombre(view.tfMunNombre.getText());
        ArrayList<ObjectId>usuarios=new ArrayList<>();
        for (int i=0; i<view.dlmMunUsuario.getSize(); i++){
            Usuario user = (Usuario) view.dlmMunUsuario.getElementAt(i);
            usuarios.add(user.getId());
        }
        mundo.setUsuarios(usuarios);
        return mundo;
    }

    private void addActionCommands() {
        view.btConectar.setActionCommand("Conectar");
        view.btAnadir.setActionCommand("Anadir");
        view.btModificar.setActionCommand("Modificar");
        view.btGuardar.setActionCommand("Guardar");
        view.btEliminar.setActionCommand("Eliminar");
        view.btCancelar.setActionCommand("Cancelar");
        view.btGuardaManual.setActionCommand("GuardaManual");
        view.btCambiar.setActionCommand("Cambiar");
        view.btMunAnadir.setActionCommand("Registrar");
        view.btMunQuitar.setActionCommand("Quitar");

        view.exportxml.setActionCommand("Exportxml");
        view.exportsql.setActionCommand("ExportSql");
        view.cambiarRutaTrabajo.setActionCommand("GuardarComo");
    }

    private void addListeners() {
        view.btConectar.addActionListener(this);
        view.btAnadir.addActionListener(this);
        view.btModificar.addActionListener(this);
        view.btGuardar.addActionListener(this);
        view.btEliminar.addActionListener(this);
        view.btCancelar.addActionListener(this);
        view.btGuardaManual.addActionListener(this);
        view.tfBuscar.addKeyListener(this);
        view.btCambiar.addActionListener(this);
        view.btMunAnadir.addActionListener(this);
        view.btMunQuitar.addActionListener(this);

        view.tabbedPane1.addChangeListener(this);
        view.lBuscador.addListSelectionListener(this);
        view.lMunPersonas.addListSelectionListener(this);
        view.lUsuMundos.addListSelectionListener(this);

        view.exportxml.addActionListener(this);
        view.exportsql.addActionListener(this);
        view.chkMagia.addChangeListener(this);
        view.cambiarRutaTrabajo.addActionListener(this);
    }

    private void activarInicial() {
        this.clave=null;
        this.posicion=0;
        this.campoVacio=false;
        view.tfUsuFecha.setEnabled(false);
        view.btAnadir.setEnabled(false);
        view.btModificar.setEnabled(false);
        view.lguardado.setVisible(false);
        view.btGuardaManual.setVisible(false);
        view.tabbedPane1.setEnabled(false);
        view.lBuscador.setModel(view.dlmUsuario);
        view.lUsuMundos.setModel(view.dlmUsuMundo);
        view.lMunPersonas.setModel(view.dlmMunUsuario);

        view.lbbdd.setVisible(false);
        view.rbMysql.setVisible(false);
        view.rbPostgre.setVisible(false);
        view.btCambiar.setVisible(false);

        view.archivo.setEnabled(false);
        view.xml.setEnabled(false);
        view.sql.setEnabled(false);
    }

    private void activarCampos(boolean b) {
        view.tfUsuNombre.setEnabled(b);
        view.tfUsuApellido.setEnabled(b);
        view.tfUsuFecha.setEnabled(b);
        view.cbUsuRaza.setEnabled(b);
        view.cbUsuClase.setEnabled(b);

        view.tfClaNombre.setEnabled(b);
        view.tfClaVida.setEnabled(b);
        view.rbPesada.setEnabled(b);
        view.rbMedia.setEnabled(b);
        view.rbLigera.setEnabled(b);
        view.chkMagia.setEnabled(b);
        view.cbClaMagia.setEnabled(b);

        view.tfRazNom.setEnabled(b);
        view.tfRazSkill1.setEnabled(b);
        view.tfRazSkill2.setEnabled(b);
        view.tfRazSkill3.setEnabled(b);
        view.cbRazSkill.setEnabled(b);

        view.tfMunNombre.setEnabled(b);
        view.btMunAnadir.setEnabled(b);
        view.btMunQuitar.setEnabled(b);
        view.lMunPersonas.setEnabled(b);
        view.cbMunJugadores.setEnabled(b);

        view.btAnadir.setEnabled(!b);
        view.tfBuscar.setEnabled(!b);
        view.cbBuscador.setEnabled(!b);
        view.lBuscador.setEnabled(!b);
        view.btGuardar.setEnabled(b);
        view.btCancelar.setEnabled(b);
    }

    private void limpiarTablas() {
        view.tfUsuNombre.setText("");
        view.tfUsuApellido.setText("");
        view.tfUsuFecha.setDate(null);

        view.tfClaNombre.setText("");
        view.tfClaVida.setText("");
        view.armor.clearSelection();
        view.chkMagia.setSelected(false);

        view.tfRazNom.setText("");
        view.tfRazSkill1.setText("");
        view.tfRazSkill2.setText("");
        view.tfRazSkill3.setText("");

        view.tfMunNombre.setText("");
        view.dlmMunUsuario.removeAllElements();
        view.dlmUsuMundo.removeAllElements();
    }

    private void limpiarcb() {
        view.cbUsuClase.removeAllItems();
        view.cbUsuRaza.removeAllItems();
        view.cbClaMagia.removeAllItems();
        view.cbRazSkill.removeAllItems();
        view.cbMunJugadores.removeAllItems();
    }

    private void refrescarcb() {
        limpiarcb();
        for (Clase clase : model.obtenerClase()) {
            view.cbUsuClase.addItem(clase);
        }

        for (Raza raza : model.obtenerRaza()){
            view.cbUsuRaza.addItem(raza);
        }

        for (Usuario usuario : model.obtenerUsuario()){
            view.cbMunJugadores.addItem(usuario);
        }

        for (Magias magia : Magias.values()){
            view.cbClaMagia.addItem(magia);
        }

        for (Habilidades hab : Habilidades.values()){
            view.cbRazSkill.addItem(hab);
        }

    }

    private void refrescardlm() {
        view.dlmUsuario.removeAllElements();
        for (Usuario usuario : model.obtenerUsuario()){
            view.dlmUsuario.addElement(usuario);
        }

        view.dlmClase.removeAllElements();
        for (Clase clase : model.obtenerClase()){
            view.dlmClase.addElement(clase);
        }

        view.dlmRaza.removeAllElements();
        for (Raza raza : model.obtenerRaza()){
            view.dlmRaza.addElement(raza);
        }
        view.dlmMundo.removeAllElements();
        for (Mundo mundo : model.obtenerMundo()){
            view.dlmMundo.addElement(mundo);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        String cadena= view.tfBuscar.getText();
        //Usuario
        if (posicion==0){
            ArrayList<Usuario>usuario=null;
            if (cadena.length()<3){
                usuario=new ArrayList<>(model.obtenerUsuario());
            }else{
                usuario=model.obtenerUsuario(cadena);
            }
            view.dlmUsuario.clear();
            for (Usuario user : usuario){
                view.dlmUsuario.addElement(user);
            }
        //Clase
        }else if (posicion==1){
            ArrayList<Clase>clase=null;
            if (cadena.length()<3){
                clase = new ArrayList<>(model.obtenerClase());
                view.dlmClase.clear();
                view.lBuscador.setEnabled(true);
                for (Clase clas : clase){
                    view.dlmClase.addElement(clas);
                }
            }else{
                //TODO
                clase = model.obtenerClase(cadena);
                view.dlmClase.clear();
                for (Usuario user : model.obtenerClaseV2(cadena)){
                    view.dlmClase.addElement(user);
                }
                view.lBuscador.setEnabled(false);
            }
        //Raza
        }else if (posicion==2){
            ArrayList<Raza> raza=null;
            if (cadena.length()<3){
                raza = new ArrayList<>(model.obtenerRaza());
            }else{
                raza = (model.obtenerRaza(cadena));
            }
            view.dlmRaza.clear();
            for (Raza raz : raza){
                view.dlmRaza.addElement(raz);
            }
        //Mundo
        } else if (posicion==3){
            ArrayList<Mundo> mundo = null;
            if (cadena.length()<3){
                mundo = new ArrayList<>(model.obtenerMundo(cadena));
            }else{
                mundo = new ArrayList<>(model.obtenerMundo(cadena));
            }
            view.dlmMundo.clear();
            for (Mundo mun : mundo){
                view.dlmMundo.addElement(mun);
            }
        }

    }
}