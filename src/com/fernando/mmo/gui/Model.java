package com.fernando.mmo.gui;

import com.fernando.mmo.base.Clase;
import com.fernando.mmo.base.Mundo;
import com.fernando.mmo.base.Raza;
import com.fernando.mmo.base.Usuario;
import com.fernando.mmo.util.*;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.io.*;
import java.util.*;

/**
 * Created by Fernando on 08/11/2016.
 */
public class Model {
    private String RUTA_BASE;
    private String RUTA_PROPS;
    private Properties config;
    private MongoClient mongoCon;
    private MongoDatabase mongoDB;

    public Model() {
        RUTA_BASE = Constantes.DEF_RUTA;
        RUTA_PROPS = RUTA_BASE + File.separator + "Config.props";
    }

    //Rellenar documentos
    private Document rellenarDocumentoUsuario(Usuario usuario) {
        Document document = new Document()
                .append("nombre", usuario.getNombre())
                .append("apellido", usuario.getApellido())
                .append("fecha", usuario.getFecha())
                .append("mundos", usuario.getMundos());
        if (usuario.getClase() != null)
            document.append("clase", usuario.getClase().getId());
        else
            document.append("clase", null);
        if (usuario.getRaza() != null)
            document.append("raza", usuario.getRaza().getId());
        else
            document.append("raza", null);

        return document;
    }

    //Registro
    public void registrar(Raza raza){
        Document documento = new Document()
                .append("raza", raza.getRaza())
                .append("skill1", raza.getSkill1())
                .append("skill2", raza.getSkill2())
                .append("skill3", raza.getSkill3())
                .append("skillex", raza.getSkillEx())
                .append("usuarios", raza.getUsuarios());
        mongoDB.getCollection("Raza").insertOne(documento);
    }

    public void registrar(Clase clase){
        Document documento = new Document()
                .append("clase", clase.getClase())
                .append("salud", clase.getSalud())
                .append("armadura", clase.getArmadura())
                .append("magia", clase.isMagia())
                .append("magias", clase.getMagias())
                .append("usuarios", clase.getUsuarios());
        mongoDB.getCollection("Clase").insertOne(documento);
    }

    public void registrar(Usuario usuario){
        Document documento = rellenarDocumentoUsuario(usuario);
        mongoDB.getCollection("Usuario").insertOne(documento);

        ObjectId a = documento.getObjectId("clase");
        if (a != null){
            Clase clase = obtenerClase(a);
            clase.getUsuarios().add(documento.getObjectId("_id"));
            modificarClase(clase);
        }
        ObjectId b = documento.getObjectId("raza");
        if (b != null){
            Raza raza = obtenerRaza(b);
            raza.getUsuarios().add(documento.getObjectId("_id"));
            modificarRaza(raza);
        }
    }

    public void registrar(Mundo mundo){
        Document documento = new Document()
                .append("nombre", mundo.getNombre())
                .append("usuarios", mundo.getUsuarios());
        mongoDB.getCollection("Mundo").insertOne(documento);
        if (mundo.getUsuarios() != null){
            for (ObjectId id : mundo.getUsuarios()){
                Usuario usuario = obtenerUsuario(id);
                usuario.getMundos().add(documento.getObjectId("_id"));
                modificarUsuario(usuario);
            }
        }
    }

    //Modificar
    public void modificarUsuario(Usuario usuario) {
        Usuario user = obtenerUsuario(usuario.getId());

        if (user.getClase() !=null){
            ObjectId a = user.getClase().getId();
            if (a != null){
                Clase clase = obtenerClase(a);
                clase.getUsuarios().remove(user.getId());
                clase.getUsuarios().add(usuario.getId());
                modificarClase(clase);
            }
        }

        if (user.getRaza() !=null){
            ObjectId b = user.getRaza().getId();
            if (b != null){
                Raza raza = obtenerRaza(b);
                raza.getUsuarios().remove(user.getId());
                raza.getUsuarios().add(usuario.getId());
                modificarRaza(raza);
            }
        }


        Document document = rellenarDocumentoUsuario(usuario);
        mongoDB.getCollection("Usuario").replaceOne(new Document("_id", usuario.getId()),document);
    }

    public void modificarClase(Clase clase) {
        mongoDB.getCollection("Clase").replaceOne(new Document("_id", clase.getId()),
                new Document()
                .append("clase", clase.getClase())
                .append("salud", clase.getSalud())
                .append("armadura", clase.getArmadura())
                .append("magia", clase.isMagia())
                .append("magias", clase.getMagias())
                .append("usuarios", clase.getUsuarios()));
    }

    public void modificarRaza(Raza raza) {
        mongoDB.getCollection("raza").replaceOne(new Document("_id",raza.getId()),
                new Document()
                .append("raza", raza.getRaza())
                .append("skill1", raza.getSkill1())
                .append("skill2", raza.getSkill2())
                .append("skill3", raza.getSkill3())
                .append("skillex", raza.getSkillEx())
                .append("usuarios", raza.getUsuarios()));
    }

    public void modificarMundo(Mundo mundo) {
        Mundo m = obtenerMundo(mundo.getId());

        if (mundo.getUsuarios() != null){
            List<ObjectId> a = mundo.getUsuarios();
            for (ObjectId obj : a){
                Usuario usuario = obtenerUsuario(obj);
                usuario.getMundos().remove(m.getId());
                usuario.getMundos().add(mundo.getId());
                modificarUsuario(usuario);
            }
        }

        mongoDB.getCollection("Mundo").replaceOne(new Document("_id", mundo.getId()),
                new Document()
                .append("nombre", mundo.getNombre())
                .append("usuarios", mundo.getUsuarios()));
    }

    //Eliminar
    public void eliminarRaza(Raza clave){
        mongoDB.getCollection("Raza").deleteOne(new Document("_id", clave.getId()));
    }

    public void eliminarClase(Clase clave){
        mongoDB.getCollection("Clase").deleteOne(new Document("_id", clave.getId()));
    }

    public void eliminarUsuario(Usuario clave){
        if (clave.getClase()!= null){
            ObjectId a = clave.getClase().getId();
            Clase clase = obtenerClase(a);
            clase.getUsuarios().remove(clave.getId());
            modificarClase(clase);
        }
        if (clave.getRaza()!= null){
            ObjectId b = clave.getRaza().getId();
            Raza raza = obtenerRaza(b);
            raza.getUsuarios().remove(clave.getId());
            modificarRaza(raza);
        }
        mongoDB.getCollection("Usuario").deleteOne(new Document("_id", clave.getId()));
    }

    public void eliminarMundo(Mundo clave){
        if (clave.getUsuarios() != null){
            for (ObjectId id : clave.getUsuarios()){
                Usuario usuario = obtenerUsuario(id);
                usuario.getMundos().remove(clave.getId());
                modificarUsuario(usuario);
            }
        }
        mongoDB.getCollection("Mundo").deleteOne(new Document("_id", clave.getId()));
    }

    //Obtener
    public Collection<Clase> obtenerClase(){
        FindIterable findIterable = mongoDB.getCollection("Clase")
                .find();
        List<Clase> clases = new ArrayList<>();
        Clase clase = null;
        Iterator<Document> iter = findIterable.iterator();
        while (iter.hasNext()){
            Document document = iter.next();
            clase = rellenarclase(clase, document);
            clases.add(clase);
        }
        return clases;
    }

    public Collection<Raza> obtenerRaza(){
        FindIterable findIterable = mongoDB.getCollection("Raza")
                .find();
        List<Raza>razas = new ArrayList<>();
        Raza raza = null;
        Iterator<Document> iter = findIterable.iterator();
        while (iter.hasNext()){
            Document document = iter.next();
            raza = rellenarRaza(raza, document);
            razas.add(raza);
        }
        return razas;
    }

    public Collection<Usuario> obtenerUsuario(){
        FindIterable findIterable = mongoDB.getCollection("Usuario")
                .find();
        List<Usuario>usuarios = new ArrayList<>();
        Usuario user = null;
        Iterator<Document> iter = findIterable.iterator();
        while (iter.hasNext()){
            Document document = iter.next();
            user = rellenarUsuario(user, document);
            usuarios.add(user);
        }
        return usuarios;
    }

    public Collection<Mundo> obtenerMundo(){
        FindIterable findIterable = mongoDB.getCollection("Mundo")
                .find();
        List<Mundo>mundos = new ArrayList<>();
        Mundo mundo = null;
        Iterator<Document> iter = findIterable.iterator();
        while (iter.hasNext()){
            Document document = iter.next();
            mundo = rellenarMundo(mundo, document);
            mundos.add(mundo);
        }
        return mundos;
    }

    //Obtener por string y campo
    public ArrayList<Usuario> obtenerUsuario(String valor) {

        Document document = new Document("$or", Arrays.asList(
                new Document("nombre", valor),
                new Document("apellido", valor),
                new Document("fecha", valor)));
        FindIterable findIterable = mongoDB.getCollection("Usuario")
                .find(document);
        Iterator<Document> iter = findIterable.iterator();
        ArrayList<Usuario> usuarios = new ArrayList<>();
        Usuario usuario = null;
        while (iter.hasNext()) {
            Document documento = iter.next();
            usuario = rellenarUsuario(usuario, documento);
            usuarios.add(usuario);
        }
        return usuarios;
    }

    public ArrayList<Clase> obtenerClase(String valor){
        Document document = new Document("$or", Arrays.asList(
                new Document("clase", valor),
                new Document("salud", valor),
                new Document("armadura", valor),
                new Document("magia", valor),
                new Document("magias", valor)));

        FindIterable iterable = mongoDB.getCollection("Clase")
                .find(document);
        Iterator<Document> iter = iterable.iterator();
        ArrayList<Clase>clases = new ArrayList<>();
        Clase clase = null;
        while (iter.hasNext()){
            Document documento = iter.next();
            clase = rellenarclase(clase, documento);
            clases.add(clase);
        }
        return clases;
    }

    public ArrayList<Usuario> obtenerClaseV2(String valor){
        Document documento = new Document("clase",valor);
        ArrayList<Usuario> usuarios = new ArrayList<>();
        FindIterable iterable = mongoDB.getCollection("Clase")
                .find(documento);
        Iterator<Document> iter = iterable.iterator();
        Document document = null;
        while(iter.hasNext()){
            document = iter.next();
            List<ObjectId>ids = (List<ObjectId>) document.get("usuarios");
            for (ObjectId id : ids){
                usuarios.add(obtenerUsuario(id));
            }
        }
        return usuarios;
    }

    public ArrayList<Raza> obtenerRaza(String valor){
        Document document = new Document("$or", Arrays.asList(
                new Document("raza ", valor),
                new Document("skill1", valor),
                new Document("skill2", valor),
                new Document("skill3", valor),
                new Document("skillex", valor)));
        FindIterable iterable = mongoDB.getCollection("Raza")
                .find(document);
        Iterator<Document>iter = iterable.iterator();
        ArrayList<Raza>razas = new ArrayList<>();
        Raza raza = null;
        while (iter.hasNext()){
            Document documento = iter.next();
            raza = rellenarRaza(raza, documento);
            razas.add(raza);
        }
        return razas;
    }

    public ArrayList<Mundo> obtenerMundo(String valor){
        return new ArrayList<>(obtenerMundo());
    }

    //Obtener por id
    public Clase obtenerClase(ObjectId clase) {
        Document document = new Document("_id", clase);
        FindIterable findIterable = mongoDB.getCollection("Clase")
                .find(document);
        Iterator<Document> iter = findIterable.iterator();
        Clase tipo= null;
        while (iter.hasNext()){
            Document document1 = iter.next();
            tipo = rellenarclase(tipo, document1);
        }
        return tipo;
    }

    public Raza obtenerRaza(ObjectId raza) {
        Document document = new Document("_id", raza);
        FindIterable findIterable = mongoDB.getCollection("Raza")
                .find(document);
        Iterator<Document> iter = findIterable.iterator();
        Raza raza1 = null;
        while (iter.hasNext()){
            Document document1 = iter.next();
            raza1= rellenarRaza(raza1, document1);
        }
        return raza1;
    }

    public Usuario obtenerUsuario (ObjectId usuario){
        Document document = new Document("_id", usuario);
        FindIterable findIterable = mongoDB.getCollection("Usuario")
                .find(document);
        Iterator<Document> iterator = findIterable.iterator();
        Usuario user = null;
        while (iterator.hasNext()){
            Document documento = iterator.next();
            user = rellenarUsuario(user, documento);
        }
        return user;
    }

    public Mundo obtenerMundo (ObjectId mundo){
        Document document = new Document("_id", mundo);
        FindIterable findIterable = mongoDB.getCollection("Mundo")
                .find(document);
        Iterator<Document> iter = findIterable.iterator();
        Mundo world = null;
        while (iter.hasNext()){
            Document documento = iter.next();
            world = rellenarMundo(world, documento);
        }
        return world;
    }

    //Rellenado
    private Raza rellenarRaza(Raza raza, Document document) {
        raza = new Raza();
        raza.setId(document.getObjectId("_id"));
        raza.setRaza(document.getString("raza"));
        raza.setSkill1(document.getString("skill1"));
        raza.setSkill2(document.getString("skill2"));
        raza.setSkill3(document.getString("skill3"));
        raza.setSkillEx(document.getString("skillex"));
        raza.setUsuarios((List<ObjectId>) document.get("usuarios"));
        return raza;
    }

    private Clase rellenarclase(Clase clase, Document document) {
        clase = new Clase();
        clase.setId(document.getObjectId("_id"));
        clase.setClase(document.getString("clase"));
        clase.setSalud(document.getDouble("salud"));
        clase.setArmadura(document.getString("armadura"));
        clase.setMagia(document.getBoolean("magia", true));
        clase.setMagias(document.getString("magias"));
        clase.setUsuarios((List<ObjectId>) document.get("usuarios"));
        return clase;
    }

    private Usuario rellenarUsuario(Usuario user, Document document) {
        user = new Usuario();
        user.setId(document.getObjectId("_id"));
        user.setNombre(document.getString("nombre"));
        user.setApellido(document.getString("apellido"));
        user.setFecha(document.getDate("fecha"));
        user.setMundos((List<ObjectId>) document.get("mundos"));
        user.setClase(obtenerClase(document.getObjectId("clase")));
        user.setRaza(obtenerRaza(document.getObjectId("raza")));
        return user;
    }

    private Mundo rellenarMundo(Mundo mundo, Document document) {
        mundo = new Mundo();
        mundo.setId(document.getObjectId("_id"));
        mundo.setNombre(document.getString("nombre"));
        mundo.setUsuarios((List<ObjectId>) document.get("usuarios"));
        return mundo;
    }

    //Ajustes de BBDD y PROPS
    public void conectarBBDD(){
        mongoCon = new MongoClient();
        mongoDB = mongoCon.getDatabase("mmoV4");
    }

    public void crearProps(String ruta) throws IOException {
        this.config = new Properties();
        config.setProperty("user_login", Constantes.DEF_USER);
        config.setProperty("user_pwd", Constantes.DEF_PWD);
        config.setProperty("admin_login", Constantes.ADMIN_USER);
        config.setProperty("admin_pwd", Constantes.ADMIN_PWD);
        config.setProperty("BBDD", Constantes.BASE_DATOS);
        config.setProperty("ruta", ruta);
        config.store(new FileOutputStream(new File(RUTA_PROPS)), "Configuracion");
        RUTA_BASE = config.getProperty("ruta");
    }

    public void comprobacionFileConfig(String ruta) throws IOException {
        File file = new File(Constantes.DEF_RUTA + File.separator + "Config.props");
        if (!file.exists()) {
            crearProps(ruta);
        }
    }

    public String comprobarUserPwd(String user, String pwd) throws IOException {
        this.config = new Properties();
        config.load(new FileInputStream(new File(RUTA_PROPS)));
        Constantes.BASE_DATOS = config.getProperty("BBDD");
        if (user.equals(config.getProperty("user_login")) && pwd.equals(config.getProperty("user_pwd"))) {
            return "usuario";
        } else if (user.equals(config.getProperty("admin_login")) && pwd.equals(config.getProperty("admin_pwd"))) {
            return "admin";
        }
        return "error";
    }
}
