package com.fernando.mmo.base;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fernando on 20/01/2017.
 */
public class Raza {
    private ObjectId id;
    private String raza;
    private String skill1;
    private String skill2;
    private String skill3;
    private String skillEx;
    private List<ObjectId> usuarios;

    public Raza() {
        usuarios = new ArrayList<>();
    }

    public List<ObjectId> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<ObjectId> usuarios) {
        this.usuarios = usuarios;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getSkill1() {
        return skill1;
    }

    public void setSkill1(String skill1) {
        this.skill1 = skill1;
    }

    public String getSkill2() {
        return skill2;
    }

    public void setSkill2(String skill2) {
        this.skill2 = skill2;
    }

    public String getSkill3() {
        return skill3;
    }

    public void setSkill3(String skill3) {
        this.skill3 = skill3;
    }

    public String getSkillEx() {
        return skillEx;
    }

    public void setSkillEx(String skillEx) {
        this.skillEx = skillEx;
    }

    @Override
    public String toString() {
        return raza;
    }
}
