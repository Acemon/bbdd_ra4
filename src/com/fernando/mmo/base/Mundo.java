package com.fernando.mmo.base;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fernando on 20/01/2017.
 */
public class Mundo {
    private ObjectId id;
    private String nombre;
    private List<ObjectId> usuarios;

    public Mundo() {
        usuarios = new ArrayList<>();
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<ObjectId> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<ObjectId> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
