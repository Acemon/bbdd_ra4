package com.fernando.mmo.base;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fernando on 20/01/2017.
 */
public class Clase {

    public enum Armadura {
        Pesada,
        Media,
        Ligera
    }

    private ObjectId id;
    private String clase;
    private Double salud;
    private String armadura;
    private Boolean magia;
    private String magias;
    private List<ObjectId> usuarios;

    public Clase() {
        usuarios = new ArrayList<>();
    }

    public List<ObjectId> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<ObjectId> usuarios) {
        this.usuarios = usuarios;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public Double getSalud() {
        return salud;
    }

    public void setSalud(Double salud) {
        this.salud = salud;
    }

    public String getArmadura() {
        return armadura;
    }

    public void setArmadura(String armadura) {
        this.armadura = armadura;
    }

    public Boolean isMagia() {
        return magia;
    }

    public void setMagia(Boolean magia) {
        this.magia = magia;
    }

    public String getMagias() {
        return magias;
    }

    public void setMagias(String magias) {
        this.magias = magias;
    }

    @Override
    public String toString() {
        return clase;
    }

}
