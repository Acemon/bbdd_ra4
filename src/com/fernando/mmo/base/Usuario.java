package com.fernando.mmo.base;

import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fernando on 20/01/2017.
 */
public class Usuario {
    private ObjectId id;
    private String nombre;
    private String apellido;
    private java.util.Date fecha;
    private List<ObjectId> mundos;
    private Clase clase;
    private Raza raza;

    public Usuario (){
        mundos=new ArrayList<>();
        clase = null;
        raza = null;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public java.util.Date getFecha() {
        return fecha;
    }

    public void setFecha(java.util.Date fecha) {
        this.fecha = fecha;
    }

    public List<ObjectId> getMundos() {
        return mundos;
    }

    public void setMundos(List<ObjectId> mundos) {
        this.mundos = mundos;
    }

    public Clase getClase() {

        return clase;
    }

    public void setClase(Clase clase) {
        this.clase = clase;
    }

    public Raza getRaza() {
        return raza;
    }

    public void setRaza(Raza raza) {
        this.raza = raza;
    }

    @Override
    public String toString() {
        return nombre+" "+apellido;
    }
}
